#!/bin/bash

set -e

THIS_SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ "$DID_UPDATE_TRANSLATIONS" = "yes" ]; then
  echo "Translations already updated in a previous step (skipping step)"
  exit 0
fi

cd $source_root_path
if [ ! -f .phraseapp.yml ] && [ ! -f .phrase.yml ]; then
  echo ".phrase.yml do not exist (skipping step)"
  exit 0
fi

# If we're not running on macOS, we need to install the phrase-cli
if [[ "$OSTYPE" != "darwin"* ]]; then
  if ! command -v phrase &> /dev/null; then
    echo "Installing phrase-cli"
    wget https://github.com/phrase/phrase-cli/releases/download/2.31.2/phrase_linux_386
    chmod +x phrase_linux_386
    sudo mv phrase_linux_386 /usr/local/bin/phrase
  fi
fi

phrase info
phrase pull

envman add --key DID_UPDATE_TRANSLATIONS --value yes
